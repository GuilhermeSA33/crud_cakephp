<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Task[]|\Cake\Collection\CollectionInterface $tasks
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Ações') ?></li>
        <li><?= $this->Html->link(__('Nova Tarefa'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('Usuários'), ['controller' => 'Users']) ?></li>
    </ul>
</nav>
<div class="tasks index large-9 medium-9 columns content">
    <h3><?= __('Tarefas') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('Id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Nome') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Conteúdo') ?></th>
                <th scope="col" class="actions"><?= __('Ações') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($tasks as $task): ?>
            <tr>
                <td><?= $this->Number->format($task->id) ?></td>
                <td><?= h($task->name) ?></td>
                <td><?= h($task->content) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('Mostrar'), ['action' => 'view', $task->id]) ?>
                    <?= $this->Html->link(__('Editar'), ['action' => 'edit', $task->id]) ?>
                    <?= $this->Form->postLink(__('Apagar'), ['action' => 'delete', $task->id], ['confirm' => __('Tem certeza que deseja apagar a tarefa  {0}?', $task->name)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('Primeira')) ?>
            <?= $this->Paginator->prev('< ' . __('Anterior')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('Próxima') . ' >') ?>
            <?= $this->Paginator->last(__('Última') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Pagína {{page}} de {{pages}}, mostrando {{current}} registro(s) de {{count}} total')]) ?></p>
    </div>
</div>
